﻿using UnityEngine;
using System.Collections;

public class MouseFreeLookMove : MonoBehaviour
{

    public float sensitivityX = 10F;
    public float sensitivityY = 10F;

    public float moveUnitsPerSecond = 1f;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    public Vector3 defaultRotation = new Vector3();
    private Quaternion defaultRotationQuat = new Quaternion();

    private float rotationY = 0F;

    private Vector3 movementVector;
    private Transform _transform; // Cached reference

    // Use this for initialization
    void Start()
    {
        // Make the rigid body not change rotation
        if (rigidbody)
            rigidbody.freezeRotation = true;

        movementVector = new Vector3();
        _transform = transform;
        resetCameraPosition();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            resetCameraPosition();
        }

        float rotationX = _transform.localEulerAngles.y;

        if (Input.GetMouseButton(1))
        {
            rotationX += Input.GetAxis("Mouse X") * sensitivityX;
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
        }

        _transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);

        movementVector.x = Input.GetAxis("GlobalX") * (moveUnitsPerSecond * Time.deltaTime);
        movementVector.y = -Input.GetAxis("GlobalY") * (moveUnitsPerSecond * Time.deltaTime);
        movementVector.z = Input.GetAxis("GlobalZ") * (moveUnitsPerSecond * Time.deltaTime);
        if (Input.GetKey(KeyCode.LeftShift))
            _transform.Translate(movementVector, Space.Self);
        else
        {
            _transform.Rotate(Vector3.left, -rotationY);
            _transform.Translate(movementVector, Space.Self);
            _transform.Rotate(Vector3.left, rotationY);
        }
    }

    void resetCameraPosition()
    {
        _transform.position = Vector3.up * 2;
        defaultRotationQuat.eulerAngles = defaultRotation;
        _transform.rotation = defaultRotationQuat;
        rotationY = defaultRotation.x;
    }
}
