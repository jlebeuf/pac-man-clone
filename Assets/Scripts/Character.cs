﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Transform))]
public class Character : MonoBehaviour {

    /// <summary>
    /// Initial value set in Inspector
    /// </summary>
    public Enums.Direction InitialTravelDirection;
    public float InitialTravelSpeed;

    private Enums.Direction travelDirection;
    public Enums.Direction TravelDirection { get { return travelDirection; } }
    private float travelSpeed;
    public float TravelSpeed { get { return travelSpeed; } }
    private int row;
    public int Row { get { return row; } }
    private int col;
    public int Col { get { return col; } }
    private float transitionPercentage;

	// Use this for initialization
	public virtual void Start () {
        travelDirection = InitialTravelDirection;
        travelSpeed = InitialTravelSpeed;
	}
	
	// Update is called once per frame
	public virtual void Update () {
	
	}

    public void SetPosition(float xPos, float yPos)
    {
        Transform t = this.gameObject.transform;
        Vector3 v = new Vector3(xPos, yPos, t.position.z);
        this.gameObject.transform.position = v;
    }
}
