﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManager : MonoBehaviour {

    private Board board;

    public GameObject PlayerPrefab;
    public GameObject EnemyPrefab;

	// Use this for initialization
	void Start () {
        board = FindObjectOfType<Board>();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (board == null)
            board = FindObjectOfType<Board>();
	}

    public void MakePlayer(int row, int col)
    {
        Instantiate(PlayerPrefab, new Vector3(row+0.5f, 0.5f, col+0.5f), Quaternion.identity);
    }

    public void MakeEnemy(int row, int col)
    {
        Instantiate(EnemyPrefab, new Vector3(row + 0.5f, 0.5f, col + 0.5f), Quaternion.identity);
    }
}
