﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardDefs {

    private List<Board.Cell[,]> boards;
    public int Count { get { return boards.Count; } }

    private static Board.Cell f = new Board.Cell(false, Board.PacDotType.None); // Filled
    private static Board.Cell n = new Board.Cell(true, Board.PacDotType.Small); // Normal
    private static Board.Cell[,] standardBoard = new Board.Cell[31, 28] {
        {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f},
        {f, n, n, n, n, n, n, n, n, n, n, n, n, f, f, n, n, n, n, n, n, n, n, n, n, n, n, f},
        {f, n, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, n, f},
        {f, n, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, n, f},
        {f, n, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, n, f},
        {f, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
        {f, n, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, n, f},
        {f, n, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, n, f},
        {f, n, n, n, n, n, n, f, f, n, n, n, n, f, f, n, n, n, n, f, f, n, n, n, n, n, n, f},
        {f, f, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, n, n, n, n, n, n, n, n, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {n, n, n, n, n, n, n, n, n, n, f, f, f, f, f, f, f, f, n, n, n, n, n, n, n, n, n, n},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, n, n, n, n, n, n, n, n, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f},
        {f, n, n, n, n, n, n, n, n, n, n, n, n, f, f, n, n, n, n, n, n, n, n, n, n, n, n, f},
        {f, n, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, n, f},
        {f, n, f, f, f, f, n, f, f, f, f, f, n, f, f, n, f, f, f, f, f, n, f, f, f, f, n, f},
        {f, n, n, n, f, f, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, f, f, n, n, n, f},
        {f, f, f, n, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, n, f, f, f},
        {f, f, f, n, f, f, n, f, f, n, f, f, f, f, f, f, f, f, n, f, f, n, f, f, n, f, f, f},
        {f, n, n, n, n, n, n, f, f, n, n, n, n, f, f, n, n, n, n, f, f, n, n, n, n, n, n, f},
        {f, n, f, f, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, f, f, n, f},
        {f, n, f, f, f, f, f, f, f, f, f, f, n, f, f, n, f, f, f, f, f, f, f, f, f, f, n, f},
        {f, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
        {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f}
    };


    public BoardDefs()
    {
        boards = new List<Board.Cell[,]>();
        Board.Cell[,] b;

        b = new Board.Cell[15, 15] {
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f}
        };
        boards.Add(b);

        b = new Board.Cell[15, 15] {
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, f, f, f, f, f, f, n, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f}
        };
        boards.Add(b);

        b = new Board.Cell[15, 15] {
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, f, f, f, f, f, n, f, f, f, f, f, n, f},
            {f, n, n, n, n, n, n, n, n, n, n, n, n, n, f},
            {f, f, f, f, f, f, f, f, f, f, f, f, f, f, f}
        };
        boards.Add(b);

        boards.Add(standardBoard);
    }

    public Board.Cell[,] GetBoardAt(int index)
    {
        if (index < 0 || index >= boards.Count)
            return null;
        else
            return boards[index];
    }
}
