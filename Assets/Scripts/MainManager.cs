﻿using UnityEngine;
using System.Collections;

public class MainManager : MonoBehaviour {

    private static int nextLevelNumber = 0;
    private static BoardDefs boardDefs = new BoardDefs();
    private CharacterManager characterManager;
    private Board board;

	// Use this for initialization
	void Start () {
        characterManager = FindObjectOfType<CharacterManager>();
        board = FindObjectOfType<Board>();
        Screen.showCursor = false;

        if (nextLevelNumber >= boardDefs.Count)
            nextLevelNumber = 0;
        Board.Cell[,] boardCells = boardDefs.GetBoardAt(nextLevelNumber++);

        if (boardCells != null)
        {
            board.SetCells(boardCells);
            characterManager.MakePlayer(1, 1);
            characterManager.MakeEnemy(1, 10);
        }
	}

    void Awake()
    {
        if (characterManager == null)
            characterManager = FindObjectOfType<CharacterManager>();
        if (board == null)
            board = FindObjectOfType<Board>();
        Screen.showCursor = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (board.PacDotCount < 1)
            loadNextLevel();

        if (Input.GetButtonDown("Pause"))
        {
            if (Time.timeScale != 1f)
            {
                Time.timeScale = 1f;
                Screen.showCursor = false;
            }
            else
            {
                Time.timeScale = 0f;
                Screen.showCursor = true;
            }
        }
	}

    private void loadNextLevel()
    {
        Application.LoadLevel("Main");
    }

    public void loadFirstLevel()
    {
        nextLevelNumber = 0;
        loadNextLevel();
    }
}
