﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Board : MonoBehaviour {

    public GameObject PacDotPrefab;

    public enum PacDotType
    {
        None = 0,
        Small,
        Large
    }

    private int pacDotCount = 0;
    public int PacDotCount { get { return pacDotCount; } }

    public class Cell : SettlersEngine.IPathNode<object> {
        public Int32 X { get; set; }
        public Int32 Y { get; set; }

        public bool IsWalkable(object unused)
        {
            return Traversible;
        }

        public bool Traversible = false;
        public PacDotType PacDot = PacDotType.None;
        public Cell(bool _traversible = false, PacDotType _pacDot = PacDotType.None)
        {
            Traversible = _traversible;
            PacDot = _pacDot;
        }
        public Cell(Cell other)
        {
            Traversible = other.Traversible;
            PacDot = other.PacDot;
        }
        public void Copy(Cell other)
        {
            Traversible = other.Traversible;
            PacDot = other.PacDot;
        }
    }

    public float textureScale = 1f;
    public int textureFilledXPos;
    public int textureFilledYPos;
    public int textureEmptyXPos;
    public int textureEmptyYPos;
    public bool drawWalls;
    public bool drawFilledTops;

    /// <summary>
    /// Initial value set in Inspector
    /// </summary>
    public int InitialRows;
    /// <summary>
    /// Initial value set in Inspector
    /// </summary>
    public int InitialCols;

    private int rows;
    public int ROWS { get { return rows; } }
    private int cols;
    public int COLS { get { return cols; } }
    /// <summary>
    /// Grid of cells, defined as [increasing row (y-coord), increasing col (x-coord)] (bottom right origin)
    /// </summary>
    private Cell[,] cells = null;

    private Mesh mesh, meshC;
    private MeshCollider meshColliderComponent;
    private List<Vector3> vertices;
    private List<int> triangles;
    private List<Vector2> uv;

    private bool drawUpdate;
    public bool alwaysDrawUpdate = false;
    private SettlersEngine.SpatialAStar<Cell, object> aStar;


	// Use this for initialization
	void Start () {
        rows = InitialRows;
        cols = InitialCols;
        cells = new Cell[rows,cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                cells[i, j] = new Cell();
                cells[i, j].X = i;
                cells[i, j].Y = j;
            }
        }
        aStar = new SettlersEngine.SpatialAStar<Cell, object>(cells);

        mesh = GetComponent<MeshFilter>().mesh;
        meshColliderComponent = GetComponent<MeshCollider>();
        meshC = new Mesh();
        vertices = new List<Vector3>();
        triangles = new List<int>();
        uv = new List<Vector2>();

        drawUpdate = true;
	}
	
	// Update is called once per frame
	void Update () {
        //if (drawUpdate)
            Draw();
	}

    void PacDotDestroyed() {
        pacDotCount--;
    }

    public void SetCells(Cell[,] newCells)
    {
        PacDot[] dots = FindObjectsOfType<PacDot>();
        foreach (PacDot dot in dots)
            UnityEngine.Object.Destroy(dot.gameObject);
        pacDotCount = 0;

        rows = newCells.GetLength(0);
        cols = newCells.GetLength(1);
        cells = new Cell[rows, cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                cells[i, j] = new Cell(newCells[i, j]);
                cells[i, j].X = i;
                cells[i, j].Y = j;
                if (PacDotPrefab != null && newCells[i, j].PacDot != PacDotType.None)
                {
                    ((GameObject)Instantiate(PacDotPrefab, new Vector3(i + 0.5f, 0f, j + 0.5f), Quaternion.identity)).transform.parent = transform;
                    pacDotCount++;
                }
            }
        }
        aStar = new SettlersEngine.SpatialAStar<Cell, object>(cells);
        drawUpdate = true;
    }

    public Cell cell(int row, int col)
    {
        if (row < 0 || row >= rows || col < 0 || col >= cols)
            return null;
        return cells[row,col];
    }

    public bool getCellPosition(Vector3 position, out int row, out int col)
    {
        row = 0;
        col = 0;

        if (position.x < 0 || position.x >= rows || position.z < 0 || position.z >= cols)
            return false;

        row = (int)position.x;
        /*if (position.x - row > 0.5f)
            row++;//*/
        col = (int)position.z;
        /*if (position.z - col > 0.5f)
            col++;//*/

        return true;
    }

    public bool pathToCell(int sourceRow, int sourceCol, int destRow, int destCol, out int headingRow, out int headingCol)
    {
        headingRow = 0;
        headingCol = 0;

        LinkedList<Cell> path = aStar.Search(new Vector2(sourceRow, sourceCol), new Vector2(destRow, destCol), null);

        if (path == null || path.First == null || path.First.Next == null)
            return false;

        headingRow = path.First.Next.Value.X;
        headingCol = path.First.Next.Value.Y;

        return true;
    }

    public bool pathToCell(Vector3 sourcePosition, Vector3 targetPosition, out Vector3 direction)
    {
        direction.x = 0;
        direction.y = 0;
        direction.z = 0;

        int sourceRow, sourceCol, destRow, destCol, headingRow, headingCol;

        if (!getCellPosition(sourcePosition, out sourceRow, out sourceCol))
            return false;
        if (!getCellPosition(targetPosition, out destRow, out destCol))
            return false;
        if (!pathToCell(sourceRow, sourceCol, destRow, destCol, out headingRow, out headingCol))
            return false;

        direction.x = (float)headingRow + 0.5f;
        direction.y = 0;
        direction.z = (float)headingCol + 0.5f;

        return true;
    }

    void Draw()
    {
        if (!alwaysDrawUpdate && !drawUpdate)
            return;
        drawUpdate = false;


        mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        vertices.Clear();
        triangles.Clear();
        uv.Clear();

        int squares = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                bool isFloor = cells[i, j].Traversible;
                int height = 0;
                if (!isFloor)
                    height = 1;
                int xl = j-1;
                int xu = j+1;
                int yl = i-1;
                int yu = i+1;
                bool XLWall = !(xl >= 0 && cells[i, xl].Traversible == true);
                bool XUWall = !(xu < cols && cells[i, xu].Traversible == true);
                bool YLWall = !(yl >= 0 && cells[yl, j].Traversible == true);
                bool YUWall = !(yu < rows && cells[yu, j].Traversible == true);

                float uvXMin;
                float uvXMax;
                float uvYMin;
                float uvYMax;

                if (isFloor)
                {
                    vertices.Add(new Vector3(i, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j));
                    vertices.Add(new Vector3(i, height, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uvXMin = textureFilledXPos*textureScale;
                    uvXMax = uvXMin+textureScale;
                    uvYMin = textureFilledYPos*textureScale;
                    uvYMax = uvYMin+textureScale;
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));
                    squares++;
                }
                else if (drawFilledTops)
                {
                    vertices.Add(new Vector3(i, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j));
                    vertices.Add(new Vector3(i, height, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uvXMin = textureEmptyXPos*textureScale;
                    uvXMax = uvXMin+textureScale;
                    uvYMin = textureEmptyYPos*textureScale;
                    uvYMax = uvYMin + textureScale;
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));
                    squares++;
                }


                if (!isFloor)
                    continue;

                if (drawWalls)
                {
                    vertices.Add(new Vector3(i, height + 1, j + 1));
                    vertices.Add(new Vector3(i + 1, height + 1, j + 1));
                    vertices.Add(new Vector3(i + 1, height + 1, j));
                    vertices.Add(new Vector3(i, height + 1, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 2);

                    uvXMin = textureFilledXPos * textureScale;
                    uvXMax = uvXMin + textureScale;
                    uvYMin = textureFilledYPos * textureScale;
                    uvYMax = uvYMin + textureScale;

                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                } else
                {
                    uvXMin = textureFilledXPos * textureScale;
                    uvXMax = uvXMin + textureScale;
                    uvYMin = textureFilledYPos * textureScale;
                    uvYMax = uvYMin + textureScale;
                }

                if (XLWall)
                {
                    vertices.Add(new Vector3(i + 1, 1, j));
                    vertices.Add(new Vector3(i, 1, j));
                    vertices.Add(new Vector3(i, 0, j));
                    vertices.Add(new Vector3(i + 1, 0, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (XUWall)
                {
                    vertices.Add(new Vector3(i, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 0, j + 1));
                    vertices.Add(new Vector3(i, 0, j + 1));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (YLWall)
                {
                    vertices.Add(new Vector3(i, 1, j));
                    vertices.Add(new Vector3(i, 1, j + 1));
                    vertices.Add(new Vector3(i, 0, j + 1));
                    vertices.Add(new Vector3(i, 0, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (YUWall)
                {
                    vertices.Add(new Vector3(i + 1, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 1, j));
                    vertices.Add(new Vector3(i + 1, 0, j));
                    vertices.Add(new Vector3(i + 1, 0, j + 1));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }
            }
        }

        meshC.Clear();
        meshC.vertices = vertices.ToArray();
        meshC.triangles = triangles.ToArray();
        meshC.RecalculateNormals();
        meshC.Optimize();
        meshColliderComponent.sharedMesh = null;
        meshColliderComponent.sharedMesh = meshC;

        // Now redraw in reverse order for transparency in all directions
        if (!drawWalls)
        {
            vertices.Clear();
            triangles.Clear();
            uv.Clear();
            squares = 0;
        }

        for (int i = rows-1; i >= 0; i--)
        {
            for (int j = cols-1; j >= 0; j--)
            {
                bool isFloor = cells[i, j].Traversible;
                int height = 0;
                if (!isFloor)
                    height = 1;
                int xl = j - 1;
                int xu = j + 1;
                int yl = i - 1;
                int yu = i + 1;
                bool XLWall = false;// !(xl >= 0 && cells[i, xl].Traversible == true);
                bool XUWall = false;// !(xu < cols && cells[i, xu].Traversible == true);
                bool YLWall = false;// !(yl >= 0 && cells[yl, j].Traversible == true);
                bool YUWall = false;// !(yu < rows && cells[yu, j].Traversible == true);


                float uvXMin;
                float uvXMax;
                float uvYMin;
                float uvYMax;

                if (isFloor)
                {
                    vertices.Add(new Vector3(i, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j));
                    vertices.Add(new Vector3(i, height, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uvXMin = textureFilledXPos * textureScale;
                    uvXMax = uvXMin + textureScale;
                    uvYMin = textureFilledYPos * textureScale;
                    uvYMax = uvYMin + textureScale;
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));
                    squares++;
                }
                else if (drawFilledTops)
                {
                    vertices.Add(new Vector3(i, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j + 1));
                    vertices.Add(new Vector3(i + 1, height, j));
                    vertices.Add(new Vector3(i, height, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uvXMin = textureEmptyXPos * textureScale;
                    uvXMax = uvXMin + textureScale;
                    uvYMin = textureEmptyYPos * textureScale;
                    uvYMax = uvYMin + textureScale;
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));
                    squares++;
                }

                if (!isFloor)
                    continue;

                uvXMin = textureFilledXPos * textureScale;
                uvXMax = uvXMin + textureScale;
                uvYMin = textureFilledYPos * textureScale;
                uvYMax = uvYMin + textureScale;

                if (XLWall)
                {
                    vertices.Add(new Vector3(i + 1, 1, j));
                    vertices.Add(new Vector3(i, 1, j));
                    vertices.Add(new Vector3(i, 0, j));
                    vertices.Add(new Vector3(i + 1, 0, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (XUWall)
                {
                    vertices.Add(new Vector3(i, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 0, j + 1));
                    vertices.Add(new Vector3(i, 0, j + 1));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (YLWall)
                {
                    vertices.Add(new Vector3(i, 1, j));
                    vertices.Add(new Vector3(i, 1, j + 1));
                    vertices.Add(new Vector3(i, 0, j + 1));
                    vertices.Add(new Vector3(i, 0, j));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }

                if (YUWall)
                {
                    vertices.Add(new Vector3(i + 1, 1, j + 1));
                    vertices.Add(new Vector3(i + 1, 1, j));
                    vertices.Add(new Vector3(i + 1, 0, j));
                    vertices.Add(new Vector3(i + 1, 0, j + 1));
                    triangles.Add((squares * 4) + 0);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 3);
                    triangles.Add((squares * 4) + 1);
                    triangles.Add((squares * 4) + 2);
                    triangles.Add((squares * 4) + 3);
                    uv.Add(new Vector2(uvXMin, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMax));
                    uv.Add(new Vector2(uvXMax, uvYMin));
                    uv.Add(new Vector2(uvXMin, uvYMin));

                    squares++;
                }
            }
        }


        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv.ToArray();

        mesh.RecalculateNormals();
        mesh.Optimize();
    }
}
