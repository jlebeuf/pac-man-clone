﻿using UnityEngine;
using System.Collections;

public class PacDot : MonoBehaviour
{

    public enum PacDotType
    {
        None = 0,
        Small,
        Large
    }

    bool active;

	// Use this for initialization
	void Start () {
        active = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (active && collider.gameObject.tag == "Player")
        {
            if (gameObject.transform.parent != null)
                gameObject.transform.parent.BroadcastMessage("PacDotDestroyed");
            UnityEngine.Object.Destroy(this.gameObject);
            active = false;
        }
    }
}
