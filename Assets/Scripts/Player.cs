﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private MainManager mainManager;

	// Use this for initialization
	void Start () {
        mainManager = FindObjectOfType<MainManager>();
	}

    void Awake() {
        mainManager = FindObjectOfType<MainManager>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Enemy")
            mainManager.loadFirstLevel();
    }
}
