﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterMotor))]
[AddComponentMenu("Character/EnemyInputController")]
public class EnemyInputController : MonoBehaviour {
    private CharacterMotor motor;
    private Rigidbody rigidBody;
    public float MovementSpeed = 0.1f;
    public bool active = true;

    void Awake()
    {
        motor = GetComponent<CharacterMotor>();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (!active)
        {
            motor.inputMoveDirection = Vector3.zero;
            return;
        }

        GameObject player = GameObject.FindWithTag("Player");
        if (player == null)
            return;
        Board board = FindObjectOfType<Board>();
        if (board == null)
            return;

        Vector3 heading;
        board.pathToCell(transform.position, player.transform.position, out heading);
        if (heading == Vector3.zero)
            return;

        heading.y = transform.position.y;
        Vector3 target = heading - transform.position;
        transform.rotation = Quaternion.LookRotation(target);

        // Get the input vector from keyboard or analog stick
        Vector3 directionVector = new Vector3(0, 0, 1);

        // Get the length of the directon vector and then normalize it
        // Dividing by the length is cheaper than normalizing when we already have the length anyway
        float directionLength = directionVector.magnitude;
        directionVector = directionVector / directionLength;

        // Make sure the length is no bigger than 1
        directionLength = Mathf.Min(1, directionLength);

        // Make the input vector more sensitive towards the extremes and less sensitive in the middle
        // This makes it easier to control slow speeds when using analog sticks
        directionLength = directionLength * directionLength;

        // Multiply the normalized direction vector by the modified length
        directionVector = directionVector * directionLength;

        // Apply the direction to the CharacterMotor
        motor.inputMoveDirection = transform.rotation * directionVector;
	}
}
