﻿using UnityEngine;
using System.Collections;

public class Enums {

    [System.Flags]
    public enum Direction
    {
        XPos,
        XNeg,
        YPos,
        YNeg
    }
}
